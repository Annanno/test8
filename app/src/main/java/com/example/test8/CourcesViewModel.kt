package com.example.test8

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.test8.model.CoursesModel
import com.example.test8.network.RetrofitInstance
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class CourcesViewModel: ViewModel() {

    var response: MutableLiveData<Response<CoursesModel>> = MutableLiveData()

    fun getData(){
        viewModelScope.launch {
            withContext(IO){
                response.postValue(RetrofitInstance.retrofit.getCources())

            }
        }


    }

}