package com.example.test8

import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.setImage(url: String?){
    if (!url.isNullOrEmpty())
        Glide.with(context).load(url).error(R.mipmap.ic_launcher).into(this)
    else
        setImageResource(R.mipmap.ic_launcher)
}