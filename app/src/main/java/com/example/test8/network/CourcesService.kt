package com.example.test8.network

import com.example.test8.model.CoursesModel
import retrofit2.Response
import retrofit2.http.GET

interface CourcesService {

    @GET("v3/4167a598-b68c-420f-b6e1-fef68b89a10d")
    suspend fun getCources(): Response<CoursesModel>
}