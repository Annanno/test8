package com.example.test8.fragments


import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.test8.CourcesViewModel
import com.example.test8.adapters.ActiveCourcesAdapter
import com.example.test8.adapters.NewCoursesAdapter
import com.example.test8.databinding.FragmentCoursesPageBinding
import kotlinx.coroutines.launch

class CoursesPageFragment : BaseFragment<FragmentCoursesPageBinding>(FragmentCoursesPageBinding::inflate) {

    private lateinit var horizontalRecycler: NewCoursesAdapter
    private lateinit var verticalRecycler: ActiveCourcesAdapter
    private val viewModel: CourcesViewModel by viewModels()

    override fun start() {
        super.start()
        viewModel.getData()
        initVerticalRecycler()
        initHorizontalRecycler()
        observe()


    }

    fun initHorizontalRecycler(){
        horizontalRecycler = NewCoursesAdapter()
        binding.horizontalRecycler.apply {
            adapter = horizontalRecycler
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        }

    }

    fun initVerticalRecycler(){
        verticalRecycler = ActiveCourcesAdapter()
        binding.verticalRecycler.apply {
            adapter = verticalRecycler
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }

    }

    fun observe(){
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.response.observe(viewLifecycleOwner,{
                horizontalRecycler.setData(it.body()!!.newCourses.toMutableList())
                verticalRecycler.setData(it.body()!!.activeCourses.toMutableList())
            })


        }
    }

}