package com.example.test8.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.test8.databinding.ActiveCourcesItemBinding
import com.example.test8.model.ActiveCourse
import com.example.test8.setImage

class ActiveCourcesAdapter: RecyclerView.Adapter<ActiveCourcesAdapter.ViewHolder>() {

    var activeCourcesList = mutableListOf<ActiveCourse>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ActiveCourcesItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun getItemCount() = activeCourcesList.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(private val binding: ActiveCourcesItemBinding) : RecyclerView.ViewHolder(binding.root){

        fun onBind(){
            val cource = activeCourcesList[adapterPosition]
            val backgroundColor = Color.parseColor("#" + cource.mainColor)
            val time = "Booked for" + cource.bookingTime
            binding.bookingTime.apply {
                text = time
                setTextColor(backgroundColor)
            }
            binding.icon.setImage(cource.image)
            binding.background.setBackgroundColor(backgroundColor)


        }

    }

    fun setData(list: MutableList<ActiveCourse>){
        activeCourcesList = list
        notifyDataSetChanged()

    }
}