package com.example.test8.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.test8.R
import com.example.test8.databinding.NewCourcesItamBinding
import com.example.test8.model.NewCourse
import java.util.concurrent.TimeUnit

class NewCoursesAdapter: RecyclerView.Adapter<NewCoursesAdapter.ViewHolder>() {

    var newCourcesList = mutableListOf<NewCourse>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
            NewCourcesItamBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

        override fun getItemCount() = newCourcesList.size


        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.onBind()
        }

        inner class ViewHolder(private val binding: NewCourcesItamBinding) : RecyclerView.ViewHolder(binding.root){

            fun onBind(){
                val cource = newCourcesList[adapterPosition]
                val backgroundColor = Color.parseColor("#" + cource.mainColor)
                val time = TimeUnit.MILLISECONDS.toSeconds(cource.duration.toLong()).toString() + " min"
                binding.title.text = cource.title
                binding.cardView.setCardBackgroundColor(backgroundColor)
                binding.question.text = cource.question
                binding.time.text = time
                if (cource.iconType == "settings")
                    binding.icon.setImageResource(R.drawable.ic_settings)
                else binding.icon.setImageResource(R.drawable.ic_card)

            }

        }

    fun setData(list: MutableList<NewCourse>){
        newCourcesList = list
        notifyDataSetChanged()

    }
    }
